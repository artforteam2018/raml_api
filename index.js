const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerDoc = require('./petstore_domain/swagger.json');

const app = express();

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));

app.listen(3000);
